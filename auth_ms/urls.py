"""auth_ms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

#Las urls son los end Point que se exponen para que un componente pueda enviar solicitudes/Son puertas que se habren para utilizarse.
#Las urls se construyen con el metodo Path(nomrbeURL,haciaDondeSeDirige); en este caso para el parametro HaciaDondeSeDirige se usa --
#el metodo INCLUDE que importa las urls desde otro modulo/aplicacion/libreria. Antes se usaban las views.
urlpatterns = [
    path('admin/', admin.site.urls),
    path('rest-auth/', include('rest_auth.urls')),
    path('rest-auth/registration/', include('rest_auth.registration.urls')),
    #path('soporte/', include('soporte.urls'))
]
#Path(nombreDeUrl,include(libreria));
#el nombreDeUrl debe incluir la libreria que se usa. En este caso rest-auth. rest-auth es un modulo de autenticacion de django.
