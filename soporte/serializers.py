from rest_framework import serializers
from .models import PQR, PersonaSoporte #Se importa la clase padre a implementar en los serializers

#Normalmente los serializer llevan el nombre del modelo.
#configuracion, con la sub clase meta le decimos al serializer que modelo tiene que usar y los campos
#que se van a extraer.

class PersonaSoporteSerializer(serializers.ModelSerializer):
    class Meta:
        model = PersonaSoporte
        fields = '__all__' #Trae todos los campos


class PQRSerializer(serializers.ModelSerializer):
    persona_soporte = PersonaSoporteSerializer(read_only=True)

    class Meta:
        model = PQR
        fields = ['persona_soporte', 'estado', 'comentario', 'created']
