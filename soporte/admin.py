from django.contrib import admin
from .models import PQR, PersonaSoporte

# Register your models here.
#para registrar los modelos
admin.site.register(PQR)
admin.site.register(PersonaSoporte)